#!/bin/bash

FILE="sales_data.csv"

if [ ! -f $FILE ]
then
  echo "Error, file couldn't be found"
  exit -1
fi

tail -n +2 $FILE| tr "," "." | sort -n -t ";" -k 2,2 -k1,1 > .tmp_sales_data.csv

counter=-1
previous=-1

echo "id;$(head -n 1 "sales_data.csv")" > sales_data_mod.csv

while read line
do
  current=$(echo $line | cut -d ';' -f2)
  if [ $current != $previous ]
  then
    previous=$current
    counter=1
  fi
  echo "$counter;$line" >> sales_data_mod.csv
  let counter=$counter+1

done < .tmp_sales_data.csv

rm .tmp_sales_data.csv
