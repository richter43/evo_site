import datetime

from app_accessDB.models import Sales, Store
from django.db import transaction

#Due to the sheer size of the csv file, a method other than reading line by line and committing every time a new object is inserted had to be found
#By creating objects in bulk and committing only at the end saves a lot of time

def run():
    with open('../EVO_Site/csv_data/store_master.csv', 'r') as lines:
        next(lines)
        for line in lines:
            array = line.split(';')
            tmp_store = Store(storeID=int(array[0]), latitude=float(array[1].replace(',','.')), \
                                longitude=float(array[2].replace(',','.')))
            tmp_store.save()
    

    with open('../EVO_Site/csv_data/sales_data_mod.csv', 'r') as lines:
        next(lines)
        last = 0
        counter = 0
        tmp_store=None
        sale_batch = []
        batch_size = 400

        for line in lines: 
            array = line.split(';')
            current=int(array[2])
            if current != last:
                last = current
                tmp_store = Store.objects.get(storeID=current)
                
            date_array = array[1].split('-')
            tmp_sale = Sales(rel_id = int(array[0]),\
                             date = datetime.date(int(date_array[0]), int(date_array[1]), int(date_array[2])), \
                             storeID = tmp_store, \
                             item = int(array[3]), \
                             qty = int(array[4]), \
                             unit_price = float(array[5]), \
                             item_category = int(array[6]))
                             
            sale_batch.append(tmp_sale)
            counter = counter + 1
            if counter == 10000:
                print("Check")
                Sales.objects.bulk_create(sale_batch, batch_size)
                sale_batch=[]
                counter = 0
    Sales.objects.bulk_create(sale_batch, batch_size)
    transaction.commit()

