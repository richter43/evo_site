import datetime

from app_accessDB.models import Sales, Store
from django.db import transaction

def run():
    with open('/home/mycroft/Sync/Workspaces/Git_Projects/Gitlab/Private/EVO_Site/csv_data/store_master.csv', 'r') as lines:
        next(lines)
        for line in lines:
            array = line.split(';')
            tmp_store = Store(storeID=int(array[0]), latitude=float(array[1].replace(',','.')), \
                                longitude=float(array[2].replace(',','.')))
            tmp_store.save()
    

    with open('/home/mycroft/Sync/Workspaces/Git_Projects/Gitlab/Private/EVO_Site/csv_data/sales_data_mod.csv', 'r') as lines:
        next(lines)
        last=0
        tmp_store=None
        for line in lines: 
            array = line.split(';')
            current=int(array[2])
            if current != last:
                last = current
                tmp_store = Store.objects.get(storeID=current)

            date_array = array[1].split('-')
            tmp_sale = Sales(rel_id = int(array[0]),\
                             date = datetime.date(int(date_array[0]), int(date_array[1]), int(date_array[2])), \
                             storeID = tmp_store, \
                             item = int(array[3]), \
                             qty = int(array[4]), \
                             unit_price = float(array[5]), \
                             item_category = int(array[6]))
                             
            tmp_sale.save()

    transaction.commit()

