BEGIN TRANSACTION;
CREATE TEMPORARY TABLE db_bak(date, storeID, item, qty, unit_price, item_category, rel_id);
INSERT INTO db_bak SELECT date, storeID, item, qty, unit_price, item_category, rel_id from app_accessDB_sales;
DROP TABLE app_accessDB_sales;
CREATE TABLE app_accessDB_sales(rel_id, storeID, date, item, qty, unit_price, item_category);
INSERT INTO app_accessDB_sales SELECT rel_id, storeID, date, item, qty, unit_price, item_category FROM db_bak;
DROP TABLE db_bak;
COMMIT;

