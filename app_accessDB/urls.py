from django.urls import path

from . import views

#Adding relative URLS and what views do they belong to
urlpatterns = [
    path('', views.index, name='index'),
    path('stores', views.stores, name='stores'),
    path('sales', views.sales, name='sales'),
    path('stores/add_store', views.add_store, name='add_store'),
    path('sales/add_sale', views.add_sale, name='add_sale'),
    path('stores/rm_store', views.remove_store, name='rm_store'),
    path('sales/rm_sale', views.remove_sale, name='rm_sale'),
    path('stores/mod_store', views.mod_store, name='mod_store'),
    path('sales/mod_sale', views.mod_sale, name='mod_sale'),
]
