import django_tables2 as tables

from .models import Store, Sales

#Basic table information

class StoreTable(tables.Table):

    class Meta:
        model=Store

class SalesTable(tables.Table):
    class Meta:
        model=Sales
