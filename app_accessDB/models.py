from django.db import models
from django.core.exceptions import ValidationError

class Store(models.Model):
    
    #Key
    #StoreID is an unique and minimal identifier for any row in the Store section, for this reason it is used as a primary_key
    storeID = models.IntegerField('Store ID', primary_key=True) 
    #Rest of the attributes
    latitude = models.FloatField('Latitude')
    longitude = models.FloatField('Longitude')

    def __str__(self): #Used for easily identifying an entry without seeing all its attributes
        return str(self.storeID)

class Sales(models.Model):
    
    #Primary key
    #Due to Django not being able to accept compound keys as a primary key, a surrogate key called 'id' has to be introduced
    #The table could be modified manually, however, the objects within the django instance would not match the rows inside the database

    #Compund Key
    #A compound key was created using two identifiers
    #Foreign key obtained from an object belonging to a Store object 
    storeID = models.ForeignKey(Store, on_delete=models.CASCADE)
    #An identifier was given to every transaction in the list depending the store in which the acquisition was done in order to keep track of 
    #individual stores
    rel_id = models.IntegerField('Transaction ID')

    #Attributes
    date = models.DateField('Transaction date')
    item = models.IntegerField('Item ID')
    qty = models.IntegerField('Sold quantity')
    unit_price = models.FloatField('Cost per unit')
    item_category = models.IntegerField("Item's category")

    class Meta:
        #Veils for the uniqueness of both the storeID and relative ID
        constraints = [
            models.UniqueConstraint(fields=['storeID', 'rel_id'], name="Primary compound key")
        ] 
