from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django_tables2 import RequestConfig
from datetime import date as dater

from .forms import *
from .models import Sales, Store
from .tables import StoreTable, SalesTable


def index(request): 
    return render(request, 'index.html')

def stores(request):
    #Initialization of a dictionary to send with the render

    context = {} 
    #Creation of a new GetStore form
    context['form'] = GetStore()
  
    #Checks whether or not the page returned anything as GET information, if so, filter the table with the provided ID
    #if not, return every single element in a table (Code can be furthermodified to make it more general, however, being a static site
    #with no need for confidentiality it is left like so until another application needs it
    if request.GET.getlist('StoreID') == [] or request.GET.getlist('StoreID')[0] == '':
        #Creates a new table with all of the elements from the class Store
        context['table'] = StoreTable(Store.objects.all())
    else:
        #Creates a new table with only the elements specified from the class Store
        context['table'] = StoreTable(Store.objects.filter(storeID=request.GET['StoreID']))

    #Reconfigures the table depending on what the returned value was
    RequestConfig(request).configure(context['table'])

    #Renders the html template by using elements within the dictionary
    return render(request, 'stores.html', context)

def sales(request):
    #Initialization of a dictionary to send with the render
    context = {} 
    #Creation of a new GetSales form
    context['form'] = GetSales()
    #Checks whether or not the page returned anything with StoreID as GET information,
    if request.GET.getlist('StoreID') == [] or request.GET.getlist('StoreID')[0] == '':
        tmp_obj = Sales.objects.all()
    else:
        tmp_obj = Sales.objects.filter(storeID=request.GET['StoreID'])

    #Checks whether or not the page returned anything with SaleID as GET information,
    if not request.GET.getlist('SaleID') == [] and not request.GET.getlist('SaleID')[0] == '':
        #Returns all the objects from class Sales that match the relative ID
        tmp_obj = tmp_obj.filter(rel_id=request.GET['SaleID'])

    #Creation of a new SalesTable object
    context['table'] = SalesTable(tmp_obj)
    
    #Reconfigures the table depending on what the returned value was
    RequestConfig(request).configure(context['table'])

    #Renders the html template by using elements within the dictionary
    return render(request, 'sales.html', context)

def add_store(request):

    #Creation of a dictionary and a form element AddStore
    context={}
    context['form'] = AddStore()

    #Checks if the site received any value from GET, if not, it returns the form to be filled
    if list(request.GET.items()) == []:
        return render(request, 'add_store.html', context)
    else:

        #Checks if the returned ID is present already in the objects of class Store
        if Store.objects.filter(storeID = request.GET['StoreID']).exists(): 
            return HttpResponse('Collision detected! (Data was not stored)')

        #If no collision is detected, the new element is added
        tmp_store = Store(storeID = request.GET['StoreID'], \
                          latitude = request.GET['Latitude'], \
                          longitude = request.GET['Longitude'])
        #Saves the object information and commits to the database 
        tmp_store.save()

        return HttpResponse('Success!')

def add_sale(request):
    
    #Creation of a dictionary and a form element AddSale
    context={}
    context['form'] = AddSale()

    #Same check as above
    if list(request.GET.items()) == []:
        return render(request, 'add_sale.html', context)
    else:

        id_temp = Sales.objects.last().id + 1
        rel_id_temp = Sales.objects.filter(storeID=request.GET['StoreID']).last().rel_id + 1
      
        date_array= []
        date_array.append(int(request.GET['date_year']))
        date_array.append(int(request.GET['date_month']))
        date_array.append(int(request.GET['date_day']))

        date_tmp = dater(date_array[0],date_array[1],date_array[2])
        #Creating a new Sales object with initial attributes obtained from the form
        tmp_sale = Sales(rel_id = rel_id_temp, \
                         storeID = Store.objects.filter(storeID=request.GET['StoreID'])[0], \
                         date = date_tmp, \
                         item = request.GET['item'], \
                         qty = request.GET['qty'], \
                         unit_price = request.GET['unit_price'], \
                         item_category = request.GET['item_category'])
        #Saves and commits to DB 
        tmp_sale.save()

        return HttpResponse('Success!')

def remove_store(request):

    context={}
    context['form'] = GetStore()

    if list(request.GET.items()) == []:
        return render(request, 'rm_store.html', context)
    else:
        
        tmp_store = get_object_or_404(Store, pk=request.GET['StoreID'])
        tmp_store.delete()

    return HttpResponse("Success!")

def remove_sale(request):

    context={}
    context['form'] = RemoveSale()

    if list(request.GET.items()) == []:
        return render(request, 'rm_sale.html', context)
    else:
        tmp_sale = get_object_or_404(Sales, pk=request.GET['ID'])
        tmp_sale.delete()

    return HttpResponse("Success!")

def mod_store(request):

    #Creation of a dictionary and a form element AddStore
    context={}
    context['form'] = ModStore()

    #Checks if the site received any value from GET, if not, it returns the form to be filled
    if list(request.GET.items()) == []:
        return render(request, 'mod_store.html', context)
    else:

        tmp_store = Store(storeID = request.GET['StoreID'], \
                          latitude = request.GET['Latitude'], \
                          longitude = request.GET['Longitude'])
        #Saves the object information and commits to the database 
        tmp_store.save()

        return HttpResponse('Success!')

def mod_sale(request):

    #Creation of a dictionary and a form element AddStore
    context={}
    context['form'] = ModSale()

    #Checks if the site received any value from GET, if not, it returns the form to be filled
    if list(request.GET.items()) == []:
        return render(request, 'mod_sale.html', context)
    else:

        date_array= []
        date_array.append(int(request.GET['date_year']))
        date_array.append(int(request.GET['date_month']))
        date_array.append(int(request.GET['date_day']))

        date_tmp = dater(date_array[0], date_array[1], date_array[2])

        tmp_sale = get_object_or_404(Sales, pk=request.GET['ID'])
        tmp_sale.date = date_tmp
        tmp_sale.item = request.GET['item']
        tmp_sale.qty = request.GET['qty']
        tmp_sale.unit_price = request.GET['unit_price']
        tmp_sale.item_category = request.GET['item_category']
        #Saves and commits to DB 
        tmp_sale.save()

        return HttpResponse('Success!')
