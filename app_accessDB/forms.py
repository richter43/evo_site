from datetime import datetime

from django import forms
from django.core.exceptions import ValidationError
from .models import Store, Sales

#Self explanatory, the information on how a form is going to be handled is contained in each class, with attributes to fill and error
#dissuading

class GetStore(forms.Form):

    StoreID = forms.ModelChoiceField(queryset=Store.objects.values_list('storeID', flat=True), help_text='Select the store identifier')

class GetSales(forms.Form):

    SaleID = forms.IntegerField(required=False)   
    StoreID = forms.ModelChoiceField(queryset=Store.objects.values_list('storeID', flat=True), help_text='Select the store identifier')

class AddStore(forms.Form):

    StoreID = forms.IntegerField()
    Latitude = forms.FloatField(min_value=-90.0, max_value=90.0)
    Longitude = forms.FloatField(min_value=-180.0, max_value=+180.0)

class AddSale(forms.Form):

    StoreID = forms.ModelChoiceField(queryset=Store.objects.values_list('storeID', flat=True)) 
    date = forms.DateField(widget=forms.SelectDateWidget(years=range(1970, (datetime.now().year+1))))
    item = forms.IntegerField(min_value=1)
    qty = forms.IntegerField(min_value=0.0)
    unit_price = forms.FloatField(min_value=0.0)
    item_category = forms.IntegerField(min_value = 1)

class RemoveSale(forms.Form):

    id_temp = Sales.objects.last().id + 1
    ID = forms.IntegerField(min_value=1, max_value=id_temp) 

class ModStore(AddStore):
   
    StoreID = forms.ModelChoiceField(queryset=Store.objects.values_list('storeID', flat=True), help_text='Select the store identifier')

class ModSale(AddSale):
    
    id_temp = Sales.objects.last().id + 1
    ID = forms.IntegerField(min_value=1, max_value=id_temp) 
    StoreID= None
