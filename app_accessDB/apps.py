from django.apps import AppConfig


class AppAccessdbConfig(AppConfig):
    name = 'app_accessDB'
